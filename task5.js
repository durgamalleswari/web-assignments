/*map*/
const odd = n => n % 2 !== 0;
const map = (f, arr) => {
  let result = [];
  for (let e of arr) {
    result.push(f(e));
  }
  return result;
};
console.log(map(odd, [1, 2, 3, 4, 5, 6, 7]));

/*filter*/
const odd1 = n => n % 2 !== 0;
const filter = (f, arr) => {
  let result = [];
  for (let e of arr) {
    if (f(e)) {
      result.push(f(e));
    }
  }
  return result;
};
console.log(filter(odd1, [1, 2, 3, 4, 5, 6, 7, 8]));

/*reduce*/
const mul = (x, y) => x * y;
const reduce = (f, arr) => {
  let result = [1];
  for (const e of arr) {
    result = f(result, e);
  }
  return result;
};
console.log(reduce(mul, [4, 3]));


/*takewhile*/
const odd2 = n => n % 2 !== 0;
const takewhile = (f, arr) => {
  const result = [];
  for (let e of arr) {
    if (f(e)) result.push(e);
    else return result;
  }
};
console.log(takewhile(odd2, [1, 3, 5, 6]));

/*dropwhile*/
const odd3 = n => n % 2 !== 0;
const dropwhile = (f, arr, n) => {
  const result = [];
  for (let i = 0; i < arr.length; i++) {
    if (f(arr[i]) === true) {
      if (f(arr[i + 1]) === false) {
        return arr.slice(i + 1);
      }
    }
  }
  return result;
};
console.log(dropwhile(odd3, [1, 3, 3, 7, 9, 5, 6]));

/*reverse*/
const reverse = arr => {
  let result = [];
  for (let i = arr.length - 1; i >= 0; i--) {
    result.push(arr[i]);
  }
  return result;
};
console.log(reverse([1, 2, 3, 4, 5]));


