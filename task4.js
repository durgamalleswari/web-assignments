/* issorted*/
const isSorted = (arr, n) => {
  const result = [];
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] > arr[i + 1]) {
      return false;
    }
  }
  return true;
};

console.log(isSorted([1, 2, 3, 4, 6, 5]));

/*every even*/
const every = arr => {
  for (let i = 0; i <= arr.length; i++)
    if (arr[i] % 2 === 0) {
    } else {
      return false;
    }
  return true;
};
console.log(every([2, 4, 6, 7, 8]));

/*any even*/
const any = (f, arr) => {
  let r = [];
  for (let e of arr) {
    r = f(e);
  }
  return r;
};
const even = i => {
  return i % 2 === 0;
};
console.log(any(even([1, 2, 3, 5, 7])));
