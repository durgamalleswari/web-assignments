
const splitAt1 = (arr, index) => {
  const first = [];
  for (let i = 0; i < index; i++) {
    first.push(arr[i]);
  }
  const second = [];
  for (let i = index; i < arr.length; i++) {
    second.push(arr[i]);
  }
  return [first, second];
};
console.log(splitAt1([1, 2, 3, 4, 5], 2));

/* pluck */
const prop = { x: 1, y: 2 };
const pluck = (obj, props) => {
  const result = {};
  for (const prop of props) {
    result[prop] = obj[prop];
  }
  return result;
};
console.log(pluck(prop, ["x"]));

/*pick*/
const pick = (arr, props) => {
  const result = [];
  for (let e of arr) {
    result.push(pluck[e]);
  }
  return result;
};
console.log(pick([{ x: 1, y: 2, z: 3 }, { x: 1, y: 2, z: 3 }], ["x", "y"]));

/*clone*/
const clone = obj => {
  const result = {};
  for (const prop in obj) {
    result[prop] = obj[prop];
  }
  return result;
};
console.log(clone({ x: 1, y: 2 }));

/*assoc*/
const assoc = (k, v, obj) => {
  const result = clone(obj);
  result[k] = v;
  return result;
};

console.log(assoc("c", 3, { a: 1, b: 2 }));

/*dissoc*/
const dissoc = (k, obj) => {
  const result = [];
  for (const i in obj) if (i !== k) result[i] = obj[i];
  return result;
};
console.log(dissoc("b", { a: 1, b: 2, c: 3 }));


/*invert*/
const raceResult = {
  first: "alice",
  second: "jake"
};
const invert = obj => {
  const result = [];
  for (let i in obj) {
    result[obj[i]] = i;
  }
  return result;
};
console.log(invert(raceResult));
